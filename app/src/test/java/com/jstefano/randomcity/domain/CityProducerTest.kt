package com.jstefano.randomcity.domain

import com.jstefano.randomcity.entity.City
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class CityProducerTest {

    lateinit var producer: CityProducer

    @Before
    fun setUp() {
        producer = CityProducer()
    }

    @Test
    fun testProducingItems() {
        // given
        val testScheduler = TestScheduler()
        val testObserver = TestObserver<City>()

        producer.changeScheduler(testScheduler)

        // when
        producer.start()
            .subscribe(testObserver)

        // then
        testScheduler.advanceTimeTo(4, TimeUnit.SECONDS)
        testObserver.assertEmpty()

        testScheduler.advanceTimeTo(6, TimeUnit.SECONDS)
        testObserver.assertValueCount(1)

        testScheduler.advanceTimeTo(11, TimeUnit.SECONDS)
        testObserver.assertValueCount(2)

        testObserver
            .assertNoErrors()
            .assertNotComplete()
    }
}