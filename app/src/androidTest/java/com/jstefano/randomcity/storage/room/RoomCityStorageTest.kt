package com.jstefano.randomcity.storage.room

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.base.Optional
import com.jstefano.randomcity.entity.City
import io.reactivex.observers.TestObserver
import org.junit.Before

import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class RoomCityStorageTest {

    lateinit var storage: RoomCityStorage

    @Before
    fun setUp() {
        val appDB = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).build()

        storage = RoomCityStorage(appDB)
    }

    @Test
    fun testGetAllOnEmptyStorage() {
        storage.getAllSortedByName()
            .firstOrError()
            .test()
            .awaitDone(5, TimeUnit.SECONDS)
            .assertValue(emptyList())
    }

    @Test
    fun testSaveAndLoad() {
        val city1 = City(name = "name1", color = "color1", date = 1)
        val city2 = City(name = "name2", color = "color2", date = 2)

        storage.saveAndWait(city1)
        storage.saveAndWait(city2)

        storage.getAllSortedByName()
            .firstOrError()
            .test()
            .awaitDone(5, TimeUnit.SECONDS)
            .assertValue(listOf(city1, city2))
    }

    @Test
    fun testGetAllSorting() {
        val city1 = City(name = "name1", color = "color1", date = 1)
        val city2 = City(name = "name2", color = "color2", date = 2)
        val city3 = City(name = "name3", color = "color3", date = 3)

        storage.saveAndWait(city2)
        storage.saveAndWait(city3)
        storage.saveAndWait(city1)

        storage.getAllSortedByName()
            .firstOrError()
            .test()
            .awaitDone(5, TimeUnit.SECONDS)
            .assertValue(listOf(city1, city2, city3))
    }

    @Test
    fun testGetAllObservableUpdates() {
        val city1 = City(name = "name1", color = "color1", date = 1)
        val city2 = City(name = "name2", color = "color2", date = 2)
        val observer = TestObserver<List<City>>()

        storage.getAllSortedByName()
            .subscribe(observer)

        observer.awaitCount(1)
        observer.assertValue(emptyList())

        storage.saveAndWait(city1)
        observer.awaitCount(2)
        observer.assertValues(emptyList(), listOf(city1))

        storage.saveAndWait(city2)
        observer.awaitCount(3)
        observer.assertValues(emptyList(), listOf(city1), listOf(city1, city2))
    }

    @Test
    fun testGetCityByNameWithoutExistingValue() {
        val city1 = City(name = "name1", color = "color1", date = 1)

        storage.saveAndWait(city1)

        storage.getCityByName("name2")
            .firstOrError()
            .test()
            .awaitDone(5, TimeUnit.SECONDS)
            .assertValue(Optional.absent())
    }

    @Test
    fun testGetCityByNameWithExistingValue() {
        val city1 = City(name = "name1", color = "color1", date = 1)
        val city2 = City(name = "name2", color = "color2", date = 2)

        storage.saveAndWait(city1)
        storage.saveAndWait(city2)

        storage.getCityByName("name2")
            .firstOrError()
            .test()
            .awaitDone(5, TimeUnit.SECONDS)
            .assertValue(Optional.of(city2))
    }

    private fun RoomCityStorage.saveAndWait(city: City) {
        save(city).test()
            .awaitDone(5, TimeUnit.SECONDS)
            .assertComplete()
    }
}