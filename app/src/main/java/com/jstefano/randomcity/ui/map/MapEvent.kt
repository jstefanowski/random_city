package com.jstefano.randomcity.ui.map

import com.google.android.gms.maps.model.LatLng

data class MapPositionEvent (
    val cityName: String,
    val latLng: LatLng
)