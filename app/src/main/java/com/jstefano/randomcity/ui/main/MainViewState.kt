package com.jstefano.randomcity.ui.main

import com.jstefano.randomcity.entity.City

data class MainViewState(
    val selectedCity: City? = null,
    val exception: Throwable? = null
)