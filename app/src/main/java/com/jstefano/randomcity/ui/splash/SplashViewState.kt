package com.jstefano.randomcity.ui.splash

data class SplashViewState(
    val isLoaded: Boolean = false,
    val exception: Throwable? = null
)