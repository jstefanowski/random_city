package com.jstefano.randomcity.ui.map

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jstefano.randomcity.domain.CitySelectionController
import com.jstefano.randomcity.domain.LocationFinder
import com.jstefano.randomcity.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class MapViewModel @ViewModelInject constructor(
    locationFinder: LocationFinder,
    citySelectionController: CitySelectionController
) : BaseViewModel<MapViewState>(MapViewState()) {

    private val _mapPosition = MutableLiveData<MapPositionEvent>()
    val mapPosition: LiveData<MapPositionEvent>
        get() = _mapPosition

    init {
        citySelectionController.selectedCity
            .filter { it.isPresent }
            .map { it.get() }
            .switchMapMaybe { city ->
                locationFinder.findByCityName(city.name)
                    .map { position -> MapPositionEvent(cityName = city.name, latLng = position) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { position -> sendMapPosition(position) },
                onError = { error -> updateState { it.copy(error = error) } }
            )
            .addTo(disposables)
    }

    private fun sendMapPosition(event: MapPositionEvent) {
        _mapPosition.postValue(event)
    }
}