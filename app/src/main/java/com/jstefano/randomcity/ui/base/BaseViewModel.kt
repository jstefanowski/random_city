package com.jstefano.randomcity.ui.base

import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel<T> constructor(
    private val initialViewState: T
): ViewModel() {

    protected val disposables = CompositeDisposable()

    private val _viewState = MutableLiveData(initialViewState)
    val viewState: LiveData<T>
        get() = _viewState

    protected fun updateState(modifier: (T) -> T) {
        val currentState = _viewState.value ?: initialViewState

        if (Looper.getMainLooper() == Looper.myLooper()) {
            _viewState.value = modifier(currentState)
        }
        else {
            _viewState.postValue(modifier(currentState))
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}