package com.jstefano.randomcity.ui.map

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.jstefano.randomcity.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapFragment : Fragment(), OnMapReadyCallback {

    private val viewModel: MapViewModel by viewModels()

    private lateinit var geocoder: Geocoder

    private var googleMap: GoogleMap? = null

    private var lastEvent: MapPositionEvent? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        geocoder = Geocoder(requireActivity())

        viewModel.mapPosition.observe(viewLifecycleOwner, Observer { moveCamera(it) })
    }

    private fun moveCamera(event: MapPositionEvent) {
        lastEvent = event

        googleMap?.apply {
            clear()
            addMarker(
                MarkerOptions()
                    .position(event.latLng)
                    .title(event.cityName)
            )
            moveCamera(CameraUpdateFactory.newLatLngZoom(event.latLng, DEFAULT_ZOOM))
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        lastEvent?.let { moveCamera(it) }
    }

    companion object {
        const val DEFAULT_ZOOM = 12f
    }
}