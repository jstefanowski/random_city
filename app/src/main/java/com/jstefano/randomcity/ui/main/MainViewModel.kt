package com.jstefano.randomcity.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import com.google.common.base.Optional
import com.jstefano.randomcity.domain.CitySelectionController
import com.jstefano.randomcity.storage.CityStorage
import com.jstefano.randomcity.ui.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class MainViewModel @ViewModelInject constructor(
    cityStorage: CityStorage,
    private val citySelectionController: CitySelectionController
) : BaseViewModel<MainViewState>(MainViewState()) {

    init {
        citySelectionController.selectedCity
            .doOnNext { selectedCity -> updateState { state -> state.copy(selectedCity = selectedCity.orNull()) } }
            .switchMap { city ->
                if (city.isPresent) {
                    cityStorage.getCityByName(city.get().name)
                } else {
                    Observable.just(Optional.absent())
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { city -> updateState { state -> state.copy(selectedCity = city.orNull()) } },
                onError = { error -> updateState { state -> state.copy(exception = error) } }
            )
            .addTo(disposables)
    }

    fun cancelCitySelection() {
        citySelectionController.setSelectedCity(null)
    }
}