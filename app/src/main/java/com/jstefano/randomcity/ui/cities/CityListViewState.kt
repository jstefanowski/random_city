package com.jstefano.randomcity.ui.cities

import com.jstefano.randomcity.entity.City

data class CityListViewState(
    val cities: List<City> = emptyList(),
    val exception: Throwable? = null
)