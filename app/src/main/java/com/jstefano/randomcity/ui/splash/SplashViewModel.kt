package com.jstefano.randomcity.ui.splash

import androidx.hilt.lifecycle.ViewModelInject
import com.jstefano.randomcity.storage.CityStorage
import com.jstefano.randomcity.ui.base.BaseViewModel
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class SplashViewModel @ViewModelInject constructor(
    cityStorage: CityStorage
) : BaseViewModel<SplashViewState>(SplashViewState()) {

    init {
        val getNonEmptyList = cityStorage.getAllSortedByName()
            .filter { it.isNotEmpty() }
            .firstOrError()
            .ignoreElement()

        val timer = Completable.timer(MIN_SPLASH_SECONDS, TimeUnit.SECONDS)

        Completable.mergeArray(getNonEmptyList, timer)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = { updateState { it.copy(isLoaded = true) } },
                onError = { error -> updateState { it.copy(exception = error) } }
            )
            .addTo(disposables)
    }

    companion object {
        const val MIN_SPLASH_SECONDS = 1L
    }
}