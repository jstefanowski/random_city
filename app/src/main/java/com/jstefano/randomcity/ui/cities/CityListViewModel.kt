package com.jstefano.randomcity.ui.cities

import androidx.hilt.lifecycle.ViewModelInject
import com.jstefano.randomcity.domain.CitySelectionController
import com.jstefano.randomcity.entity.City
import com.jstefano.randomcity.storage.CityStorage
import com.jstefano.randomcity.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class CityListViewModel @ViewModelInject constructor(
    cityStorage: CityStorage,
    private val citySelectionController: CitySelectionController
) : BaseViewModel<CityListViewState>(
    CityListViewState()
) {

    init {
        cityStorage.getAllSortedByName()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { cities -> updateState { state -> state.copy(cities = cities) } },
                onError = { error -> updateState { state -> state.copy(exception = error) } }
            )
            .addTo(disposables)
    }

    fun onCitySelected(city: City) {
        citySelectionController.setSelectedCity(city)
    }
}