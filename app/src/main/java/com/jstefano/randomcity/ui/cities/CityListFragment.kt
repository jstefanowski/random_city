package com.jstefano.randomcity.ui.cities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.jstefano.randomcity.R
import com.jstefano.randomcity.entity.City
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class CityListFragment : Fragment() {

    private lateinit var cityRecycler: RecyclerView

    private lateinit var cityAdapter: CityAdapter

    private var snackbar: Snackbar? = null

    private val viewModel: CityListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_city_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cityRecycler = view.findViewById(R.id.recyclerView)
        cityRecycler.layoutManager = LinearLayoutManager(requireActivity())

        cityAdapter = CityAdapter { viewModel.onCitySelected(it) }
        cityRecycler.adapter = cityAdapter

        viewModel.viewState.observe(viewLifecycleOwner, Observer { state -> renderState(state) })
    }

    private fun renderState(viewState: CityListViewState?) {
        if (viewState == null) return

        renderCities(viewState.cities)
        renderError(viewState.exception)
    }

    private fun renderCities(cities: List<City>) {
        cityAdapter.submitList(cities)
    }

    private fun renderError(exception: Throwable?) {
        if (exception == null) {
            val snackbar = this.snackbar
            if (snackbar != null && snackbar.isShownOrQueued) {
                snackbar.dismiss()
                this.snackbar = null
            }
        } else {
            if (snackbar == null) {
                val message = getString(R.string.error_cities_load, exception)
                val snackbar = Snackbar.make(cityRecycler, message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.button_close) { requireActivity().finish() }
                    .apply { show() }
                this.snackbar = snackbar
            }
        }
    }
}