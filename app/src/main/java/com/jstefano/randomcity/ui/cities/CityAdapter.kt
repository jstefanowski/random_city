package com.jstefano.randomcity.ui.cities

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jstefano.randomcity.R
import com.jstefano.randomcity.entity.City
import java.text.DateFormat
import java.util.*


class CityAdapter(
    val onCityClicked: (City) -> Unit
) : ListAdapter<City, CityVH>(DIFF_UTIL) {

    private val dateFormatter = DateFormat.getDateTimeInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityVH {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_city, parent, false)
        return CityVH(view)
    }

    override fun onBindViewHolder(holder: CityVH, position: Int) {
        val city = getItem(position)
        holder.cityName.text = city.name
        holder.date.text = dateFormatter.format(Date(city.date))

        val textColor = Color.parseColor(city.color)
        val swatch = Palette.Swatch(textColor, 1)
        holder.cityName.setTextColor(textColor)
        holder.cityName.setBackgroundColor(swatch.titleTextColor)

        holder.itemView.setOnClickListener { onCityClicked(city) }
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<City>() {
            override fun areItemsTheSame(oldItem: City, newItem: City): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: City, newItem: City): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class CityVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val cityName: TextView = itemView.findViewById(R.id.cityName)
    val date: TextView = itemView.findViewById(R.id.date)
}