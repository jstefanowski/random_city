package com.jstefano.randomcity.ui.map

import com.jstefano.randomcity.entity.City

data class MapViewState(
    val city: City? = null,
    val error: Throwable? = null
)
//TODO not needed anymore