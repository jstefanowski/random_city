package com.jstefano.randomcity.ui.main

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jstefano.randomcity.R
import com.jstefano.randomcity.entity.City
import com.jstefano.randomcity.ui.cities.CityListFragment
import com.jstefano.randomcity.ui.map.MapFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    private var isTwoPane = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        isTwoPane = findViewById<View>(R.id.rightFragmentContainer) != null

        if (isTwoPane) {
            var listFragment = supportFragmentManager.findFragmentByTag(FRAGMENT_LIST)
            var mapFragment = supportFragmentManager.findFragmentByTag(FRAGMENT_MAP)

            clearBackStack()
            removeFragments(listFragment, mapFragment)

            if (listFragment == null) {
                listFragment = CityListFragment()
            }

            if (mapFragment == null) {
                mapFragment = MapFragment()
            }

            supportFragmentManager
                .beginTransaction()
                .replace(R.id.leftFragmentContainer, listFragment, FRAGMENT_LIST)
                .replace(R.id.rightFragmentContainer, mapFragment, FRAGMENT_MAP)
                .commit()

        } else {
            val needsSetup = savedInstanceState == null
                    || savedInstanceState.getBoolean(SAVED_STATE_WAS_TWO_PANE, true)

            if (needsSetup) {
                var listFragment = supportFragmentManager.findFragmentByTag(FRAGMENT_LIST)
                val mapFragment = supportFragmentManager.findFragmentByTag(FRAGMENT_MAP)

                removeFragments(listFragment, mapFragment)

                if (listFragment == null) {
                    listFragment = CityListFragment()
                }

                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.stackContainer, listFragment, FRAGMENT_LIST)
                    .commit()
            }
        }

        viewModel.viewState.observe(this, Observer { state -> renderState(state) })
    }

    private fun clearBackStack() {
        repeat(supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    private fun removeFragments(vararg fragments: Fragment?) {
        var tx = supportFragmentManager.beginTransaction()
        var hasAnyFragment = false

        for (fragment in fragments) {
            if (fragment != null) {
                hasAnyFragment = true
                tx = tx.remove(fragment)
            }
        }

        if (hasAnyFragment) {
            tx.commitNow()
        }
    }

    private fun renderState(viewState: MainViewState?) {
        if (viewState == null) return

        renderToolbar(viewState.selectedCity)
        displayMapFragment(viewState.selectedCity)
    }

    private fun renderToolbar(city: City?) {
        val actionBar = supportActionBar!!
        if (city != null) {
            actionBar.title = city.name
            val color = Color.parseColor(city.color)
            actionBar.setBackgroundDrawable(ColorDrawable(color))
        }
        else {
            actionBar.setTitle(R.string.app_name)
            val color = ResourcesCompat.getColor(resources, R.color.colorPrimary, theme)
            actionBar.setBackgroundDrawable(ColorDrawable(color))
        }
    }

    private fun displayMapFragment(city: City?) {
        if (isTwoPane) return

        if (city != null && supportFragmentManager.findFragmentByTag(FRAGMENT_MAP) == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.stackContainer, MapFragment(), FRAGMENT_MAP)
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onBackPressed() {
        if (!isTwoPane && supportFragmentManager.findFragmentByTag(FRAGMENT_MAP) != null) {
            viewModel.cancelCitySelection()
        }
        super.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(SAVED_STATE_WAS_TWO_PANE, isTwoPane)
        super.onSaveInstanceState(outState)
    }

    companion object {
        const val FRAGMENT_MAP = "map_fragment"
        const val FRAGMENT_LIST = "list_fragment"

        const val SAVED_STATE_WAS_TWO_PANE = "saved_state_was_two_pane"
    }
}
