package com.jstefano.randomcity.domain

import androidx.annotation.VisibleForTesting
import com.jstefano.randomcity.entity.City
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CityProducer @Inject constructor() {

    private var scheduler = Schedulers.computation()

    fun start(): Observable<City> {
        return Observable.interval(5, TimeUnit.SECONDS, scheduler)
            .map {
                City(
                    name = cities.random(),
                    color = colors.random(),
                    date = System.currentTimeMillis()
                )
            }
    }

    @VisibleForTesting
    fun changeScheduler(s: Scheduler) {
        scheduler = s
    }

    companion object {
        private val cities = listOf ( "Gdańsk" , "Warszawa" , "Poznań" , "Białystok" , "Wrocław" , "Katowice" , "Kraków" )
        private val colors = listOf ( "Yellow" , "Green" , "Blue" , "Red" , "Black" , "White" )
    }
}