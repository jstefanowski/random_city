package com.jstefano.randomcity.domain

import com.google.common.base.Optional
import com.jstefano.randomcity.entity.City
import dagger.hilt.android.scopes.ActivityRetainedScoped
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

@ActivityRetainedScoped
class CitySelectionController @Inject constructor() {

    private val subject = BehaviorSubject.create<Optional<City>>()
    val selectedCity: Observable<Optional<City>>
        get() = subject

    fun setSelectedCity(city: City?) {
        subject.onNext(Optional.fromNullable(city))
    }
}