package com.jstefano.randomcity.domain

import android.content.Context
import android.location.Geocoder
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Maybe
import javax.inject.Inject

class LocationFinder @Inject constructor(
    @ApplicationContext context: Context
) {

    private val geocoder = Geocoder(context)

    fun findByCityName(cityName: String): Maybe<LatLng> {
        return Maybe.fromCallable {
            val results = geocoder.getFromLocationName(cityName, 1)
            if (results.isEmpty()) {
                null
            }
            else {
                LatLng(results[0].latitude, results[0].longitude)
            }
        }
    }
}