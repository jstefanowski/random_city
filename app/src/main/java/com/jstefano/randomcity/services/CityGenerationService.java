package com.jstefano.randomcity.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.jstefano.randomcity.domain.CityProducer;
import com.jstefano.randomcity.entity.City;
import com.jstefano.randomcity.storage.CityStorage;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

@AndroidEntryPoint
public class CityGenerationService extends Service {

    private Disposable disposable;

    @Inject
    CityProducer cityProducer;

    @Inject
    CityStorage cityStorage;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("Service created");

        disposable = cityProducer.start()
                .doOnNext(this::logNewCity)
                .flatMapCompletable(cityStorage::save)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::logProducingComplete,
                        this::reportError
                );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null) {
            disposable.dispose();
        }

        Timber.d("Service destroyed");
    }

    private void logNewCity(City city) {
        Timber.d("Generated city: %s", city);
    }

    private void logProducingComplete() {
        Timber.d("Stopped producing");
    }

    private void reportError(Throwable error) {
        Timber.w(error);
        Toast.makeText(this, "Producer error: " + error.toString(), Toast.LENGTH_LONG).show();
        stopSelf();
    }
}
