package com.jstefano.randomcity.storage

import com.google.common.base.Optional
import com.jstefano.randomcity.entity.City
import io.reactivex.Completable
import io.reactivex.Observable

interface CityStorage {

    fun save(city: City): Completable

    fun getAllSortedByName(): Observable<List<City>>

    fun getCityByName(name: String): Observable<Optional<City>>
}