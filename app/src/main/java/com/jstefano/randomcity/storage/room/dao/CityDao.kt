package com.jstefano.randomcity.storage.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.google.common.base.Optional
import com.jstefano.randomcity.entity.City
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface CityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(city: City): Completable

    @Query("SELECT * FROM City ORDER BY name ASC")
    fun getAllSortedByName(): Observable<List<City>>

    @Query("SELECT * FROM City WHERE name = :name")
    fun getCityByName(name: String): Observable<Optional<City>>
}