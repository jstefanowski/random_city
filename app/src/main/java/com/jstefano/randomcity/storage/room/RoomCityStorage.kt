package com.jstefano.randomcity.storage.room

import com.google.common.base.Optional
import com.jstefano.randomcity.entity.City
import com.jstefano.randomcity.storage.CityStorage
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class RoomCityStorage @Inject constructor(
    db: AppDatabase
) : CityStorage {

    private val dao = db.cityDao()

    override fun save(city: City): Completable {
        return dao.insert(city)
    }

    override fun getAllSortedByName(): Observable<List<City>> {
        return dao.getAllSortedByName()
    }

    override fun getCityByName(name: String): Observable<Optional<City>> {
        return dao.getCityByName(name)
    }
}