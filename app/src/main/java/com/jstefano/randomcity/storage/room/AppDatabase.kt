package com.jstefano.randomcity.storage.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jstefano.randomcity.entity.City
import com.jstefano.randomcity.storage.room.dao.CityDao


@Database(
    version = 2,
    entities = [ City::class ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cityDao(): CityDao

    companion object {
        private const val NAME = "app_database.db"

        @JvmStatic
        fun createDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}