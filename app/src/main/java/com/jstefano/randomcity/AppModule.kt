package com.jstefano.randomcity

import android.content.Context
import com.jstefano.randomcity.storage.CityStorage
import com.jstefano.randomcity.storage.room.AppDatabase
import com.jstefano.randomcity.storage.room.RoomCityStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class AppModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
        AppDatabase.createDatabase(context)

    @Provides
    fun provideCityStorage(appDatabase: AppDatabase): CityStorage =
        RoomCityStorage(appDatabase)
}